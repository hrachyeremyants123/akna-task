import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import AlbumsTable from "../../components/albums/AlbumsTable";
import './Albums.css';

import * as userSelectors from '../../store/user/selectors';
import * as userActions from '../../store/user/actions';

import * as albumsSelectors from '../../store/albums/selectors';
import * as albumsActions from '../../store/albums/actions';

class Albums extends Component {

    constructor(props) {
        super(props);
        this.navigateTo = this.navigateTo.bind(this)
    }

    componentDidMount() {
        const {userId} = this.props.match.params;
        if (!this.props.users.length) {
            this.props.onFetchAllUsers()
                .then(() => {
                    const user = this.props.users.find(item => {
                        return item.id.toString() === userId
                    });
                    this.props.onSetUser(user);
                    this.props.onFetchAllAlbums(userId)
                })
        } else {
            this.props.onFetchAllAlbums(userId)
        }
    }

    navigateTo(e, path) {
        e.preventDefault();
        this.props.history.push(path)
    }

    render() {
        return (
            <div className="albums">
                <AlbumsTable
                    user={this.props.user}
                    data={this.props.albums}
                    browserHistory={this.props.history}
                    onClickAlbum={this.props.onSetAlbum}
                />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        albums: albumsSelectors.getAllAlbums(state),
        user: userSelectors.getUser(state),
        users: userSelectors.getAllUsers(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchAllAlbums: (userId) => dispatch(albumsActions.fetchAll(userId)),
        onSetUser: (user) => dispatch(userActions.setUser(user)),
        onFetchAllUsers: () => dispatch(userActions.fetchAll()),
        onSetAlbum: (album) => dispatch(albumsActions.setAlbum(album))
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Albums));
