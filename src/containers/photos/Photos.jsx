import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

import PhotosList from "../../components/photos/PhotosList";
import './Photos.css'

import * as userSelectors from '../../store/user/selectors';
import * as userActions from '../../store/user/actions';

import * as albumsSelectors from '../../store/albums/selectors';
import * as albumsActions from '../../store/albums/actions';

import * as photosSelectors from '../../store/photos/selectors';
import * as photosActions from '../../store/photos/actions';

class Photos extends Component {
    componentDidMount() {
        const {userId, albumId} = this.props.match.params;
        if (!this.props.users.length) {
            this.props.onFetchAllUsers()
                .then(() => {
                    const user = this.props.users.find(item => {
                        return item.id.toString() === userId
                    });
                    this.props.onSetUser(user);
                    this.props.onFetchAllAlbums(userId)
                        .then(() => {
                            const album = this.props.albums.find(item => {
                                return item.id.toString() === albumId
                            });
                            this.props.onSetAlbum(album);
                        })
                });

        }
        this.props.onFetchAllPhotos(userId, albumId)
    }

    render() {
        return (
            <div className="photos">
                <PhotosList browserHistory={this.props.history} data={this.props.photos}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        albums: albumsSelectors.getAllAlbums(state),
        user: userSelectors.getUser(state),
        users: userSelectors.getAllUsers(state),
        album: albumsSelectors.getAlbum(state),
        photos: photosSelectors.getPhotos(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchAllAlbums: (userId) => dispatch(albumsActions.fetchAll(userId)),
        onSetUser: (user) => dispatch(userActions.setUser(user)),
        onFetchAllUsers: () => dispatch(userActions.fetchAll()),
        onSetAlbum: (album) => dispatch(albumsActions.setAlbum(album)),
        onFetchAllPhotos: (userId, albumId) => dispatch(photosActions.fetchAll(userId, albumId))
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Photos));
