import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import './Users.css';

import 'antd/dist/antd.css';
import UsersTable from '../../components/users/UsersTable'

import * as userActions from '../../store/user/actions';
import * as userSelectors from '../../store/user/selectors';

class Users extends Component {

    componentDidMount() {
        this.props.onFetchAllUsers();
    }

    render() {
        return (
            <div className="users">
                <UsersTable
                    browserHistory={this.props.history}
                    data={this.props.users}
                    onClickUser={this.props.onSetUser}
                />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        users: userSelectors.getAllUsers(state)
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchAllUsers: () => dispatch(userActions.fetchAll()),
        onSetUser: (user) => dispatch(userActions.setUser(user))
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Users));
