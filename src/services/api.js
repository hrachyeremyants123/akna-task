import axios from 'axios';
import env from '../environment';

const API_ENDPOINT = env.apiEndpoint;

class API {
    /**
     * @param resource
     * @param params
     * @return {Promise.<*>}
     */
    async get(resource, params = new Map()) {
        let options = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };

        let url = API_ENDPOINT + resource + '?' + this.serializeParams(params);

        let response = await this.request(url, options);
        if (response.status !== 200) {
            console.log(response);
        }
        return response;
    }

    /**
     * @param resource
     * @param params
     * @return {Promise.<*>}
     */
    async post(resource, params = new Map()) {
        let options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            data: params
        };

        let url = API_ENDPOINT + resource;

        let response = await this.request(url, options);
        if (response.status !== 200) {
            console.log(response);
        }
        return response;
    }

    /**
     * @param resource
     * @param params
     * @return {Promise.<*>}
     */
    async postFiles(resource, params = new Map()) {
        let formData = new FormData();
        params.forEach((value, key) => {
            formData.append(key, value);
        });

        let options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
            },
            body: formData,
        };

        let url = API_ENDPOINT + resource;

        let response = await this.request(url, options);
        if (response.status !== 200) {
            console.log(response);
        }
        return response;
    }

    /**
     * @param resource
     * @param params
     * @return {Promise.<*>}
     */
    async put(resource, params = new Map()) {
        let options = {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: this.serializeParams(params)
        };

        let url = API_ENDPOINT + resource;

        let response = await this.request(url, options);
        if (response.status !== 200) {
            console.log(response);
        }
        return response;
    }

    /**
     * @param resource
     * @param params
     * @return {Promise.<*>}
     */
    async delete(resource, params = new Map()) {
        let options = {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: this.serializeParams(params)
        };

        let url = API_ENDPOINT + resource;

        let response = await this.request(url, options);
        if (response.status !== 200) {
            console.log(response);
        }
        return response;
    }

    /**
     * @param url
     * @param options
     * @return {Promise.<*>}
     */
    async request(url, options) {
        return await axios(url, options);
    }

    /**
     * @param params
     * @return {string}
     */
    serializeParams(params) {
        let array = [];

        params.forEach((value, key) => {
            array.push(key + '=' + encodeURIComponent(value));
        });

        return array.join('&');
    }
}

export default new API();