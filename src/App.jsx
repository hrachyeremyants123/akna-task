import React, {Component} from 'react'
import {Provider} from 'react-redux';
import {Route, Switch, Redirect, Router} from 'react-router-dom';
import {createBrowserHistory} from 'history';

import Users from './containers/users/Users';
import Photos from "./containers/photos/Photos";
import Albums from "./containers/albums/Albums";

import configureStore from "./store/configureStore";

const store = configureStore();

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router history={createBrowserHistory()}>
                    <Switch>
                        <Route exact component={Users} path={'/users'}/>
                        <Route component={Albums} path={'/users/:userId/albums'}/>
                        <Route component={Photos} path={'/users/:userId/:albumId/photos'}/>
                        <Redirect from='*' to='/users'/>
                    </Switch>
                </Router>
            </Provider>
        )
    }
}