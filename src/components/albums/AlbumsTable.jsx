import React, {Component} from 'react';
import {Table, Button} from 'antd';
import PropTypes from 'prop-types';

import {columns} from "../../constants/albums/albumTableColumns";

export default class AlbumsTable extends Component {
    static propTypes = {
        data: PropTypes.array.isRequired,
        browserHistory: PropTypes.object.isRequired,
        onClickAlbum: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.onClickRowHandler = this.onClickRowHandler.bind(this);
    }

    onClickRowHandler(album) {
        return {
            onClick: () => {
                this.props.onClickAlbum(album);
                this.props.browserHistory.push(`/users/${this.props.user.id}/${album.id}/photos`)
            }
        };
    }

    render() {
        return (
            <div>
                <Button onClick={() => this.props.browserHistory.goBack()}>Back</Button>
                <Table onRow={this.onClickRowHandler}
                       columns={columns} dataSource={this.props.data}/>
            </div>
        )
    }
}

