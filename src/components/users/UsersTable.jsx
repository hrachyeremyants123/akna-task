import React, {Component} from 'react';
import {Table} from 'antd';
import PropTypes from 'prop-types';

import {columns} from "../../constants/users/usersTableColumns";

export default class UsersTable extends Component {
    static propTypes = {
        data: PropTypes.array.isRequired,
        browserHistory: PropTypes.object.isRequired,
        onClickUser: PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);
        this.onClickRowHandler = this.onClickRowHandler.bind(this);
    }

    onClickRowHandler(user) {
        return {
            onClick: () => {
                this.props.onClickUser(user);
                this.props.browserHistory.push(`/users/${user.id}/albums`)
            }
        };
    }

    render() {
        return (
            <Table onRow={this.onClickRowHandler}
                   columns={columns} dataSource={this.props.data}/>
        )
    }
}

