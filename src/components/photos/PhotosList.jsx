import React, {Component} from 'react';
import {List, Avatar, Button} from 'antd';
import PropTypes from 'prop-types';

export default class PhotosList extends Component {

    static propTypes = {
        data: PropTypes.array.isRequired,
        browserHistory: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.renderItem = this.renderItem.bind(this);
    }

    renderItem(item) {
        return (
            <List.Item
                key={item.title}
                extra={<img width={272} alt="logo"
                            src={item.url}/>}
            >
                <List.Item.Meta
                    avatar={<Avatar src={item.thumbnailUrl}/>}
                    title={<span>{item.title}</span>}
                />
            </List.Item>
        )
    }

    render() {
        return (
            <div>
                <Button onClick={() => this.props.browserHistory.goBack()}>Back</Button>
                <List
                    itemLayout="vertical"
                    size="large"
                    pagination={{
                        pageSize: 3,
                    }}
                    dataSource={this.props.data}
                    renderItem={this.renderItem}
                />
            </div>
        )
    }
}