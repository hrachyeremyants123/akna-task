export const columns = [
    {
        title: 'Id',
        dataIndex: 'id',
        sorter: (a, b) => a.id - b.id
    },
    {
        title: 'Name',
        dataIndex: 'name',
        sorter: (a, b) => a.name.length - b.name.length
    },
    {
        title: 'Email',
        dataIndex: 'email'
    },
    {
        title: 'User name',
        dataIndex: 'username'
    },
    {
        title: 'Phone number',
        dataIndex: 'phone'
    },
    {
        title: 'Address',
        dataIndex: 'address'
    },
    {
        title: 'Zip Code',
        dataIndex: 'zipCode'
    },
    {
        title: 'Website',
        dataIndex: 'website'
    }
];