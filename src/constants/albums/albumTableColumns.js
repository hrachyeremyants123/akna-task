export const columns = [
    {
        title: 'Id',
        dataIndex: 'id',
        sorter: (a, b) => a.id - b.id
    },
    {
        title: 'Title',
        dataIndex: 'title'
    },
    {
        title: 'User Name',
        dataIndex: 'userName',
        sorter: (a, b) => a.name.length - b.name.length
    }
];