import users from './user/reducer';
import albums from './albums/reducer';
import photos from './photos/reducer';

export default {
    users,
    albums,
    photos
}