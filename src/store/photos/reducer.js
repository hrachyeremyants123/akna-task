import {types} from './actions';
import Immutable from 'seamless-immutable';

const initialState = Immutable({
    photos: []
});

const setFetchedPhotos = (state, payload) => {
    return state.merge({
        photos: payload.photos
    })
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case types.FETCH_ALL:
            return setFetchedPhotos(state, action.payload);
        default:
            return state
    }
}