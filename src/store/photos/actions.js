import api from '../../services/api';

export const types = {
    FETCH_ALL: 'photos.FETCH_ALL'
};

export const fetchAll = (userId, albumId) => {
    return async dispatch => {
        try {
            const params = new Map();
            params.set('albumId', albumId);
            const photos = await api.get(`users/${userId}/photos`, params);
            dispatch(setFetchedUsers(photos.data));
        } catch (e) {
            console.log(e);
        }
    }
};

export const setFetchedUsers = (photos) => {
    return {
        type: types.FETCH_ALL,
        payload: {
            photos
        }
    }
};
