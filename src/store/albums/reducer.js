import {types} from './actions';
import Immutable from 'seamless-immutable';

const initialState = Immutable({
    albums: [],
    album: null
});

const setFetchedAlbums = (state, payload) => {
    return state.merge({
        albums: payload.albums
    })
};
const setAlbum = (state, payload) => {
    return state.merge({
        album: payload.album
    })
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case types.FETCH_ALL:
            return setFetchedAlbums(state, action.payload);
        case types.SET_ALBUM:
            return setAlbum(state, action.payload);
        default:
            return state
    }
}