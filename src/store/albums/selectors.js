import _ from 'lodash';

export const getAllAlbums = (state) => _.map(state.albums.albums, item => item.merge({
    userName: state.users.user.name,
    key: item.id.toString()
}));

export const getAlbum = (state) => state.albums.album;