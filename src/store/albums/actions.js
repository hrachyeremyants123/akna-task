import api from '../../services/api';

export const types = {
    FETCH_ALL: 'albums.FETCH_ALL',
    SET_ALBUM: 'albums.SET_ALBUM'
};

export const fetchAll = (userId) => {
    return async dispatch => {
        try {
            const albums = await api.get(`users/${userId}/albums`);
            dispatch(setFetchedAlbums(albums.data))
        } catch (e) {
            console.log(e);
        }
    }
};

export const setFetchedAlbums = (albums) => {
    return {
        type: types.FETCH_ALL,
        payload: {
            albums
        }
    }
};

export const setAlbum = (album) => {
    return {
        type: types.SET_ALBUM,
        payload: {
            album
        }
    }
};