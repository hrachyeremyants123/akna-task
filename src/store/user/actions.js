import api from '../../services/api';

export const types = {
    FETCH_ALL: 'users.FETCH_ALL',
    SET_USER: 'users.SET_USER'
};

export const fetchAll = () => {
    return async dispatch => {
        try {
            const users = await api.get('users');
            dispatch(setFetchedUsers(users.data));
        } catch (e) {
            console.log(e);
        }
    }
};

export const setFetchedUsers = (users) => {
    return {
        type: types.FETCH_ALL,
        payload: {
            users
        }
    }
};

export const setUser = (user) => {
    return {
        type: types.SET_USER,
        payload: {
            user
        }
    }
};