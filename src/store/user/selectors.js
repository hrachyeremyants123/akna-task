import _ from 'lodash';

export const getAllUsers = (state) => _.map(state.users.users, item => item.merge({
    key: item.id.toString(),
    address: `${item.address.city}, ${item.address.street} ${item.address.suite}`,
    zipCode: item.address.zipcode
}));

export const getUser = (state) => state.users.user;