import {types} from './actions';
import Immutable from 'seamless-immutable';

const initialState = Immutable({
    users: [],
    user: null
});

const setFetchedUsers = (state, payload) => {
    return state.merge({
        users: payload.users
    })
};
const setUser = (state, payload) => {
    return state.merge({
        user: payload.user
    })
};

export default (state = initialState, action = {}) => {
    switch (action.type) {
        case types.FETCH_ALL:
            return setFetchedUsers(state, action.payload);
        case types.SET_USER:
            return setUser(state, action.payload);
        default:
            return state
    }
}